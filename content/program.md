---
title: Programs
description: The 1st Asian Xenopus Conference on Nov. 24~26, 2024 at Osaka, Japan
date: 2024-08-21
---

{{< image src="/AXC2024-banner-1.jpg" alt="AXC 2024 banner" float="center">}}

## Schedule at a glance

{{< image src="/AXC-2024-program-glance.2024_11_12.jpg" alt="AXC 2024 schedule" float="center">}}

* The detailed program is available at [AXC2024-program.v3.pdf](/AXC2024-program.v3.pdf) (updated on 2024-11-24).
  * The post-doc(green) and student(blue) presenters, candidates for the award, are highlighted.
* Full abstract book is available at [here](https://1drv.ms/b/s!ApCK5F_SHa3jjt0ayejDvIPDvG4PmQ?e=GIxkKq) (required a password).

## Confirmed speakers

### Keynote speakers
* Ken Cho (University of California, Irvine)
* John B. Wallingford (University of Texas, Austin)

### PI speakers
* Atsushi Suzuki(Hiroshima University)
* Mikiko Tanaka	(Institute of Science Tokyo)
* Yoshihiro Morishita	(RIKEN Center for Biosystems Dynamics Research)
* Yuta Tanizaki	(The University of Tokyo)
* Yusuke Mii	(Kyoto University)
* Hui Chen (University of South Carolina)
* Yuki Shibata (Nippon Medical School)
* Asako Shindo (Osaka University)
* Hosung Jung	(Yonsei University)
* Masanori Taira (Chuo University)
* Tatsuo Michiue (University of Tokyo)
* Soichiro Kato	(Osaka University)
* Kee-Beom Kim (Kyungpook National University)
* Hajime Ogino (Hiroshima University)
* Taejoon Kwon (UNIST)
* Hye Ji Cha (Dankook University)
* Jun-Yeong Lee (Kyungpook National University)

### Junior PI/trainee speakers
* Ha Eun Kim (UNIST)
* Seongmin Yun (UNIST)
* Linda Choubani	(RIKEN)
* Hyeyoon Lee	(DKFZ, Germany)
* Hyun-Kyung Lee (Kyungpook National University)
* Hyo Jung Sim (UNIST)
* Yuuri Yasuoka	(RIKEN)
* Minako Suzuki	(Kyoto University/NIBB)
* Hongchan Lee (Kyungpook National University)
* Akane Kawaguchi (Research Institute of Molecular Pathology)

## Instruction for Presentation

We have allocated sufficient time for all participants who requested an oral presentation. 
If you selected "Yes, for oral presentation" during registration, 
please prepare your slides for an oral presentation. 
Below are the instructions for both oral and poster presentations.
 
### Oral presentations
* Please bring your own laptop and an HDMI connector for your presentation. 
* The screen ratio is 16:9 at both venues. 4:3 is also acceptable. 
* **All presentations should be in English, and 15 minutes for presentation followed by 5 minutes for discussion**.

### Poster presentations
* Poster board size is **900 mm (width) x 1,532 mm (height)**, which can accept an **A0 size** poster. 
* Please mount your poster on Day 2 morning. 
* Poster presentations will be assigned on Day 2 (details to be announced later). 
* Please remove your poster by the end of the conference.
