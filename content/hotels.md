---
title: "Hotels"
description: The 1st Asian Xenopus Conference on Nov. 24~26, 2024 at Osaka, Japan
date: 2024-09-10
summary: " "
---

{{< image src="/AXC2024-banner-3.jpg" alt="AXC 2024 banner" float="center">}}


## Hotels 
Hotel accommodation is not included in the registration. 
Please make your own hotel reservations through the hotel website. 

* [Senri-Hankyu Hotel](https://www.hankyu-hotel.com/hotel/hh/senrihh)

This hotel offers discounted rates for conference attendees if you book before September 23rd (Twin room for single use: JYP 20,030/night on Nov. 23rd, JYP 13,880/night from Nov. 24th to 26th). When booking this hotel, please email front@senri-htl.co.jp and mention that you will attend the Asian Xenopus Conference. After September 24th, please use the regular reservation system through the website. 
 
The following hotels are also close to Osaka University.

* [Osaka Airterminal Hotel](https://www.osaka-airterminal-hotel.com/english/)
* [Green Rich Hotels](https://greenrichhotels.jp/osaka/)
* [Toyoko Inn Osaka Itami Airport](https://www.toyoko-inn.com/index.php/search/detail/00241/)
* [Minami Senri Crystal Hotel (need to take a train)](https://crystalhotel.jp/minamisenri/)

There are many hotels in the Shin-Osaka and Umeda area. If you want to enjoy the central area of Osaka, Umeda is the most exciting place.

## Transportation
You can take the Osaka Monorail from these hotels to Nambu-Yoichiro Hall at Osaka University. 
https://www.osaka-monorail.co.jp/language/en/

From the Umeda or Shin-Osaka area, take [the Midosuji Line](https://www.osakastation.com/the-midosuji-subway-line/), which directly connects to [the Kita-Osaka Kyuko line](https://www.kita-kyu.co.jp/en). Get off at Senri-Chuo station and then transfer to [the Osaka Monorail](https://www.osaka-monorail.co.jp/language/en/).

Another option is to take the Hankyu Line from Umeda station and get off at Ishibashi-Handai Mae Station. It’s a 20-minute walk from this Station to the hall.

Taxi is also an option. 
* You can use ["GO"](https://go.goinc.jp) to call a taxi in Japan.
* The ["Kakao T"](https://www.kakaocorp.com/page/service/service/KakaoT) may also work in Osaka, Japan.
