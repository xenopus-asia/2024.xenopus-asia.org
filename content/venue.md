---
title: "Venue"
description: The 1st Asian Xenopus Conference on Nov. 24~26, 2024 at Osaka, Japan
date: 2024-06-23
summary: " "
---

{{< image src="/AXC2024-banner-3.jpg" alt="AXC 2024 banner" float="center">}}

## Conference Venue Day 1

**The venue of Day 1 has been changed!!!**

Science Hall(**5th floor**), Senri Life Science Center, Osaka, Japan

{{< image src="/science-hall.jpg" width="800px" alt="Science Hall" float="center">}}

![From Senri Chuo Station](/science-hall-direction.800x.jpg)

* How to access: https://www.senrilc.co.jp/access/english.html
* For more information: https://www.senrilc.co.jp/hall/sciencehall.html


## Conference Venue Day 2 and Day 3

Nambu Yoichiro Hall, Osaka University (Toyonaka campus), Japan 

{{< image src="/nambu-yoichiro-hall.jpg" width="800px" alt="Nambu Yoichiro Hall" float="center">}}

* https://www.frc.sci.osaka-u.ac.jp/en/facilities (also the source of the above image).

## How to access

https://www.frc.sci.osaka-u.ac.jp/en/access

![From Shibahara handai-mae Station](/station-to-hall.jpg)
* From Shibahara handai-mae Station [Link to Google Map](https://maps.app.goo.gl/16UXjVC67qgnerLF7)
  * 15 min by walk 

![From Kansai International Airport](/kansai-to-hall.jpg)
* From Kansai International Airport [Link to Google Map](https://maps.app.goo.gl/8fQbMPtcbvRH1hKw7)
  * 2 hours by public transportaiton; 1 hour by car

