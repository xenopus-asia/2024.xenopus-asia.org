---
title: Registration
description: The 1st Asian Xenopus Conference on Nov. 24~26, 2024 at Osaka, Japan
date: 2024-09-11
---

{{< image src="/AXC2024-banner-2.jpg" alt="AXC 2024 banner" float="center">}}

## Registration

Please use the following form to register the AXC 2024: 

[AXC 2024 registration form](https://docs.google.com/forms/d/e/1FAIpQLSdJNzYzB-3jQt_KZIUQqR9VLHNfQrISsIbBPzoIcfHwTDEWeQ/viewform?usp=sf_link)

The due date for the registration is **Oct. 12, 2024 (Sat)** (this is also the due date for the abstract submission).

After submitting the registration form, please send your abstract (either for a poster or an oral presentation) to xenopus.asia@gmail.com if you want to present.

The payment system is still in progress. We will email you an update on how to pay the registration fee shortly.

### Registration fee

Registration fees are as follows (including the reception in Day 1):

* PI/Faculty: 10,000 JPY
* Postdoc/Researcher: 6,000 JPY
* Student: 2,000 JPY

We would greatly appreciate it if you could pay by the end of October to help us with our budget management. 
Please note that cancellations and refunds cannot be processed after payment. 

If you have any questions about payment (or other options for the registration fees), please contact Yuuri Yasuoka (yuuri.yasuoka (at) riken (dot) jp).

