---
title: Asian Xenopus Conference 2024
description: The 1st Asian Xenopus Conference on Nov. 24~26, 2024 at Osaka, Japan
---
{{< image frame="true" src="/AXC2024-poster.2024_06_20.jpg" width="400px" alt="AXC 2024 poster" float="right">}}


**Welcome!**

This is a website for the 1st Asian _Xenopus_ Conference 2024!

We are still working on the registration/abstract submission system 
(fully functional website will be availabe in early September).

If you have any question, please contact any of organizer by email.
Contact information is available on "[contact]({{< ref "/contact" >}}) page.

**Key information**
  * Date: November 24, 2024 (Sun) to November 26, 2024 (Tues)
  * Place: Osaka University, Osaka, Japan.
  * Due date for the abstract: October 12, 2024 (**EXTENDED**). 

The PDF version of the AXC 2024 is available [AXC 2024 PDF poster](/AXC-2024-poster.2024_06_23.pdf).

## Committee Members
 * Taejoon Kwon (UNIST, Republic of Korea)
 * Tae Joo Park (UNIST, Republic of Korea)
 * Asako Shindo (Osaka University, Japan)
 * Yuuri Yasuoka (RIKEN, Japan)
 * Soichiro Kato (Osaka University, Japan)
 * Kee-Beom Kim (Kyungpook National University, Republic of Korea)
 * Yusuke Mii (Kyoto University, Japan)
 * Haruki Ochi (Yamagata University, Japan)


## Hosts
 * [KNU G-LAMP Project](https://lamp.knu.ac.kr/)
 * [Korean Society for Molecular and Cellular Biology (KSMCB)](https://www.ksmcb.or.kr/eng/)
 * Xenopus Community in Japan (XCIJ)
 * [National BioResource Project - Clawed frogs/Newts (NBRP)](https://xenopus.nbrp.jp/NBRP_Xenopus/NBRP_Clawed_frogs_Newts_Top_EN.html)

## Sponsors
 * [KNU G-LAMP Project](https://lamp.knu.ac.kr/)
 * [National BioResource Project - Clawed frogs/Newts (NBRP)](https://xenopus.nbrp.jp/NBRP_Xenopus/NBRP_Clawed_frogs_Newts_Top_EN.html)
 * [Suntory Foundation for Life Sciences](https://www.sunbor.or.jp/en/about/)
 * [IWAKI](https://iwakiamerica.com/about-iwakiamerica-japan/)
 * [Development, Growth & Differentiation (DGD)](https://onlinelibrary.wiley.com/journal/1440169X)
 * [Meito-suien](https://meito-suien.com/)
 * [Chugai-Souyaku](https://c-finds.com/en/)

 {{< image src="/sponsor_logos-2024_10_23.jpg" alt="the logos of sponsors" float="center" >}}
