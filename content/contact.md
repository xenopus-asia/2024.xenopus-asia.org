---
title: Contact information
description: The 1st Asian Xenopus Conference on Nov. 24~26, 2024 at Osaka, Japan
date: 2024-06-23
---

{{< image src="/AXC2024-banner-4.jpg" alt="AXC 2024 banner" float="center">}}

If you have any question for the conference, please contact to the following organizing committee members:

* [Taejoon Kwon](https://bme.unist.ac.kr/portfolio/faculty/faculty_taejoon-kwon/), UNIST, Republic of Korea
* [Tae Joo Park](https://unistdml.wixsite.com/my-site/professor-1), UNIST, Republic of Korea
* [Asako Shindo](https://sites.google.com/view/shindo-lab-en/), Osaka University, Japan
* [Yuuri Yasuoka](https://www.xenbase.org/xenbase/community/viewPerson.do?method=display&personId=4778), RIKEN, Japan

