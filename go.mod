module gitlab.com/pages/hugo

go 1.22

require (
	github.com/gevhaz/hugo-theme-notrack v0.0.0-20241018093114-14d307d7b843 // indirect
)
